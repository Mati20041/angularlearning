import {Component} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-myform',
  templateUrl: 'myform.component.html',
})
export class MyformComponent {

  arrayForm = this.builder.array([
    this.builder.control(''),
    this.builder.control(''),
  ]);
  groupFormWithArrayForm = this.builder.group({
    arrayForm: this.arrayForm,
  });
  formGroupWithComplex = new FormGroup({
    complex: new FormControl()
  });

  constructor(private builder: FormBuilder) {
    this.formGroupWithComplex.valueChanges.subscribe(change => {
      console.log(`formGroupWithComplex = ${JSON.stringify(change)}`);
    });
  }

  fuck = () => this.arrayForm.push(this.builder.control(''));

}
