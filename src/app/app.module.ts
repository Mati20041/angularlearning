import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyformComponent } from './myform/myform.component';
import {ReactiveFormsModule} from '@angular/forms';
import { ComplexFormComponent } from './complex-form/complex-form.component';

@NgModule({
  declarations: [
    AppComponent,
    MyformComponent,
    ComplexFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
