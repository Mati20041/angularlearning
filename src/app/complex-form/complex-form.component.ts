import {Component, forwardRef, OnInit} from '@angular/core';
import {ControlValueAccessor, FormBuilder, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'complex-form',
  templateUrl: './complex-form.component.html',
  styleUrls: ['./complex-form.component.sass'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => ComplexFormComponent),
    }
  ]
})
export class ComplexFormComponent implements ControlValueAccessor {

  value = {
    a: 'a',
    b: 'b'
  };
  valueForm = this.fb.group({
    a: this.fb.control(''),
    b: this.fb.control(''),
  });
  private onChange: any;
  private onTouched: any;

  constructor(private fb: FormBuilder) {
    this.valueForm.setValue(this.value);
    this.valueForm.valueChanges.subscribe(change => {
      this.value = change;
      if (this.onChange) {  this.onChange(this.value); }
    });
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

}
